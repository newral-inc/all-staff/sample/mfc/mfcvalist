// MFCVAList.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"
#include <afxwin.h>

#define BUFFER_SIZE	(1024)

CString GetMessage(CString fmt, ...)
{
    // 可変長引数を1つの文字列にまとめます。
    va_list args;
    va_start(args, fmt);
    TCHAR buffer[BUFFER_SIZE + 1];
	int bufferLen = sizeof(buffer);
    SecureZeroMemory(&buffer[0], bufferLen);
    _vstprintf(buffer, bufferLen, fmt, args);
    va_end(args);

    return buffer;
}

int _tmain(int argc, _TCHAR* argv[])
{
	CString fmt = _T("%s %s");
	CString msg = GetMessage(fmt, _T("Hello"), _T("World"));
	OutputDebugString(msg);
	return 0;
}

